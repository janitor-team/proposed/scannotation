#!/bin/sh -e

VERSION=$1
TAR=../scannotation_$VERSION.orig.tar.xz
DIR=scannotation-$VERSION
DATE=$(echo "$VERSION" | sed -e 's/^.*\+svn//;s/$/T2359Z/')
TAG=$(echo "scannotation-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export -r {$DATE} svn://svn.code.sf.net/p/scannotation/code/ $DIR
cp debian/License.txt $DIR
XZ_OPT=--best tar -c -J -f $TAR \
    --exclude '*.jar' \
    --exclude '*.class' \
    --exclude '*.ipr' \
    --exclude '*.iml' \
    --exclude '.settings' \
    --exclude '.project' \
    --exclude '.classpath' \
    --exclude '*.zip' \
    $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
